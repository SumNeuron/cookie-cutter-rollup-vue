# Cookie Cutter Rollup Vue

This is a cookie cutter project for using rollup and vue.


## How to get started

1. clone me
2. `cd` into this directory and run `npm install`
3. launch a simple server `python3 -m http.server <port-specification>`



## Overview of folders and their purpose

- assets: store static files for use in demos
- build: the rolled-up files end up here
- demos: directory for making and storing demos
- dependencies: local versions of dependencies to enable off-line development
- src: the place where you code
