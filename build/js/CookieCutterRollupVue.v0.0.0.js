(function () {
  'use strict';

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  var script = {
    name: 'ButtonPlus',
    props: {
      color: {
        default: 'success',
        type: String
      },
      background: {
        default: 'light',
        type: String
      }
    },
    methods: {
      plus: function plus() {
        this.$emit('plus');
      }
    }
  };

  /* script */
  var __vue_script__ = script;

  /* template */
  var __vue_render__ = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { on: { click: _vm.plus } }, [_c("i", {
      staticClass: "fa fa-plus-circle",
      class: ["bg-" + _vm.background, "text-" + _vm.color]
    })]);
  };
  var __vue_staticRenderFns__ = [];
  __vue_render__._withStripped = true;

  /* style */
  var __vue_inject_styles__ = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-126844d5_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: { "version": 3, "sources": [], "names": [], "mappings": "", "file": "ButtonPlus.vue" }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__ = undefined;
  /* module identifier */
  var __vue_module_identifier__ = undefined;
  /* functional template */
  var __vue_is_functional_template__ = false;
  /* component normalizer */
  function __vue_normalize__(template, style, script$$1, scope, functional, moduleIdentifier, createInjector, createInjectorSSR) {
    var component = (typeof script$$1 === 'function' ? script$$1.options : script$$1) || {};

    // For security concerns, we use only base name in production mode.
    component.__file = "/home/sumner/Projects/cookie-cutter-rollup-vue/src/components/ButtonPlus/ButtonPlus.vue";

    if (!component.render) {
      component.render = template.render;
      component.staticRenderFns = template.staticRenderFns;
      component._compiled = true;

      if (functional) component.functional = true;
    }

    component._scopeId = scope;

    {
      var hook = void 0;
      if (style) {
        hook = function hook(context) {
          style.call(this, createInjector(context));
        };
      }

      if (hook !== undefined) {
        if (component.functional) {
          // register for functional component in vue file
          var originalRender = component.render;
          component.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context);
          };
        } else {
          // inject component registration as beforeCreate hook
          var existing = component.beforeCreate;
          component.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }
    }

    return component;
  }
  /* style inject */
  function __vue_create_injector__() {
    var head = document.head || document.getElementsByTagName('head')[0];
    var styles = __vue_create_injector__.styles || (__vue_create_injector__.styles = {});
    var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());

    return function addStyle(id, css) {
      if (document.querySelector('style[data-vue-ssr-id~="' + id + '"]')) return; // SSR styles are present.

      var group = isOldIE ? css.media || 'default' : id;
      var style = styles[group] || (styles[group] = { ids: [], parts: [], element: undefined });

      if (!style.ids.includes(id)) {
        var code = css.source;
        var index = style.ids.length;

        style.ids.push(id);

        if (isOldIE) {
          style.element = style.element || document.querySelector('style[data-group=' + group + ']');
        }

        if (!style.element) {
          var el = style.element = document.createElement('style');
          el.type = 'text/css';

          if (css.media) el.setAttribute('media', css.media);
          if (isOldIE) {
            el.setAttribute('data-group', group);
            el.setAttribute('data-next-index', '0');
          }

          head.appendChild(el);
        }

        if (isOldIE) {
          index = parseInt(style.element.getAttribute('data-next-index'));
          style.element.setAttribute('data-next-index', index + 1);
        }

        if (style.element.styleSheet) {
          style.parts.push(code);
          style.element.styleSheet.cssText = style.parts.filter(Boolean).join('\n');
        } else {
          var textNode = document.createTextNode(code);
          var nodes = style.element.childNodes;
          if (nodes[index]) style.element.removeChild(nodes[index]);
          if (nodes.length) style.element.insertBefore(textNode, nodes[index]);else style.element.appendChild(textNode);
        }
      }
    };
  }
  /* style inject SSR */

  var ButtonPlus = __vue_normalize__({ render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ }, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, __vue_create_injector__, undefined);

  var CookieCutterRollupVue = {};

  CookieCutterRollupVue.ButtonPlus = ButtonPlus;

  // Make the library available from the windowf
  window.CookieCutterRollupVue = CookieCutterRollupVue;

}());
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29va2llQ3V0dGVyUm9sbHVwVnVlLnYwLjAuMC5qcyIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvbXBvbmVudHMvQnV0dG9uUGx1cy9CdXR0b25QbHVzLnZ1ZSIsIi4uLy4uL3NyYy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyI8dGVtcGxhdGU+XG4gIDxkaXZcbiAgICBAY2xpY2s9J3BsdXMnXG4gID5cbiAgICA8aVxuICAgICAgY2xhc3M9J2ZhIGZhLXBsdXMtY2lyY2xlJ1xuICAgICAgOmNsYXNzPSdbXCJiZy1cIitiYWNrZ3JvdW5kLCBcInRleHQtXCIrY29sb3JdJ1xuICAgICAgPlxuICAgIDwvaT5cbiAgPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICBuYW1lOiAnQnV0dG9uUGx1cycsXG4gIHByb3BzOiB7XG4gICAgY29sb3I6IHtcbiAgICAgIGRlZmF1bHQ6J3N1Y2Nlc3MnLFxuICAgICAgdHlwZTogU3RyaW5nXG4gICAgfSxcbiAgICBiYWNrZ3JvdW5kOiB7XG4gICAgICBkZWZhdWx0OiAnbGlnaHQnLFxuICAgICAgdHlwZTogU3RyaW5nXG4gICAgfVxuICB9LFxuICBtZXRob2RzOiB7XG4gICAgcGx1czogZnVuY3Rpb24oKSB7XG4gICAgICB0aGlzLiRlbWl0KCdwbHVzJyk7XG4gICAgfVxuICB9XG59O1xuPC9zY3JpcHQ+XG5cbjxzdHlsZT5cblxuPC9zdHlsZT5cbiIsInZhciBDb29raWVDdXR0ZXJSb2xsdXBWdWUgPSB7fTtcblxuaW1wb3J0IEJ1dHRvblBsdXMgZnJvbSAnLi9jb21wb25lbnRzL0J1dHRvblBsdXMvQnV0dG9uUGx1cy52dWUnO1xuXG5Db29raWVDdXR0ZXJSb2xsdXBWdWUuQnV0dG9uUGx1cyA9IEJ1dHRvblBsdXM7XG5cblxuLy8gTWFrZSB0aGUgbGlicmFyeSBhdmFpbGFibGUgZnJvbSB0aGUgd2luZG93Zlxud2luZG93LkNvb2tpZUN1dHRlclJvbGx1cFZ1ZSA9IENvb2tpZUN1dHRlclJvbGx1cFZ1ZTtcbiJdLCJuYW1lcyI6WyJDb29raWVDdXR0ZXJSb2xsdXBWdWUiLCJCdXR0b25QbHVzIiwid2luZG93Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBYUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBQUE7OztFQVZZLDJCQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQ0haLElBQUlBLHdCQUF3QixFQUE1Qjs7RUFJQUEsc0JBQXNCQyxVQUF0QixHQUFtQ0EsVUFBbkM7O0VBR0E7RUFDQUMsT0FBT0YscUJBQVAsR0FBK0JBLHFCQUEvQjs7OzsifQ==
