# Dependencies

Using a CDN is fine when working online. However, sometimes wifi is not available.

Store dependencies needed to work offline here.
